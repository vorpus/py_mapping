import oauth2 as oauth
import urllib
import json
import numpy
from numpy import array
from array import *
import csv
image = urllib.URLopener()

maps = []
file = open('coords.csv','r')
coordm = numpy.loadtxt(file, dtype={'names': ('city','lat','long'),'formats': ('S20','f2','f2')}, delimiter=',')

print 'size = '+str(coordm.size)
print 'shape = '+str(coordm.shape)
print '[1] = '+str(coordm[1])
print '[1][1] = '+str(coordm[1][1])


for x in range(0,coordm.size):
    image.retrieve("http://maps.googleapis.com/maps/api/staticmap?center="+
                   str(coordm[x][1])+","+str(coordm[x][2])+
                   "&zoom=12&size=1000x1000&format=png&maptype=satellite",str(coordm[x][0])+".png")

file.close()
